package Contest636Div3;

import java.util.Scanner;
import java.io.PrintWriter;

public class BalancedArray {
    public void solve(int testNumber, Scanner in, PrintWriter out) {
        int t = in.nextInt();
        while(t-->0){
            int n = in.nextInt();
            int [] nums = new int[n];
            if(n%4 != 0){
                out.println("NO");
            }
            else{
                int sum_pos = 0;
                int even = 2;
                for(int i=0;i<n/2;i++){
                    nums[i] = even;
                    sum_pos +=even;
                    even = even+2;
                }
                int sum_neg = 0;
                int no_even = 1;
                for (int i=n/2;i<n-1;i++){
                    nums[i] = no_even;
                    sum_neg += no_even;
                    no_even = no_even+2;
                }
                int resto = sum_pos-sum_neg;
                if(resto%2 != 0){
                    sum_neg +=resto;
                    if(sum_neg == sum_pos){
                        nums[n-1] = resto;
                        out.println("YES");
                        for (int i=0;i<nums.length;i++){
                            if(i == nums.length-1){
                                out.println(nums[i]);
                            }
                            else{
                                out.print(nums[i] +" ");
                            }
                        }
                    }
                }
                else{
                    out.println("NO");
                }
            }
        }
    }
}
