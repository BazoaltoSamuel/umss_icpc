package codes;

import java.util.Scanner;
import java.io.PrintWriter;

public class Spreadsheet {
    public void solve(int testNumber, Scanner in, PrintWriter out) {
        int t = in.nextInt();
        String[] abc = {"A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X",
                "Y","Z"};
        while(t-->0){
            String cod = in.next();
            String c1, n1, c2, n2;
            c1 = c2 = n1 = n2 = "";
            int band = 0;
            for(int i=0;i<cod.length();i++){
                char a = cod.charAt(i);
                if(band == 0){
                    c1 += a;
                    if(Character.isDigit(cod.charAt(i+1)))band++;
                }
                else if(band == 1){
                    n1 += a;
                    if(i+1 <= cod.length()-1 && !Character.isDigit(cod.charAt(i+1)))band++;
                }
                else if(band == 2){
                    band++;
                }
                else{
                    n2 += a;
                }
            }
            String res = "";
            if(!n2.equals("")){
                int div = Integer.parseInt(n2);
                while( div > 26 && div/26 < 26){
                    res = String.valueOf(abc[(div%26)-1])+res;
                    div = div/26;
                }
                res = String.valueOf(abc[(div)-1])+res;
                res += n1;
            }
            else{
                res += "R"+n1+"C";
                int cal =0;
                for(int i=0;i<c1.length();i++){
                    int n = 0;
                    for(int j =0;j<abc.length;j++){
                        if(String.valueOf(c1.charAt(i)).equals(abc[j])){
                            n = j+1;
                            break;
                        }
                    }
                    if(i==c1.length()-1){
                        cal += n;
                    }
                    else{
                        cal = (cal+n)*26;
                    }
                }
                res += String.valueOf(cal);
            }
            out.println(res);
        }
    }
}
