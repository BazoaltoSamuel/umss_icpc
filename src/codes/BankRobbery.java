package codes;

import java.util.Scanner;
import java.io.PrintWriter;

public class BankRobbery {
    public void solve(int testNumber, Scanner in, PrintWriter out) {
        int a = in.nextInt();
        int b = in.nextInt();
        int c = in.nextInt();
        int n = in.nextInt();
        int res =0;
        for(int i=0;i<n;i++){
            int x = in.nextInt();
            if(x>b && x<c)res++;
        }
        out.println(res);
    }
}
