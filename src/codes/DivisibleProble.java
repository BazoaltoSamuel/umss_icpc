package codes;

import java.util.Scanner;
import java.io.PrintWriter;

public class DivisibleProble {
    public void solve(int testNumber, Scanner in, PrintWriter out) {
        int t = in.nextInt();
        while(t-->0){
            int a = in.nextInt();
            int b = in.nextInt();
            if(a%b==0)out.println(0);
            else{
                int c = a/b;
                while((c*b)<a)c++;
                out.println((c*b)-a);
            }
        }
    }
}
