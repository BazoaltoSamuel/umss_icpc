package RCP;

import java.util.Scanner;
import java.io.PrintWriter;

public class BallparkEstimate {
    public void solve(int testNumber, Scanner in, PrintWriter out) {
        String n = in.next();
        long first_digit = Long.parseLong(n.charAt(0)+"");
        long num_min = first_digit;
        long num_max = first_digit+1;
        for(int i=0;i<n.length()-1;i++){
            num_max*=10;
            num_min*=10;
        }
        long num = Long.parseLong(n);
        if((num-num_min) < (num_max-num)) out.println(num_min);
        else out.println(num_max);
    }
}
